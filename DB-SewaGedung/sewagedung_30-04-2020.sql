-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 30, 2020 at 01:10 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sewagedung`
--

-- --------------------------------------------------------

--
-- Table structure for table `building`
--

CREATE TABLE `building` (
  `buildingId` varchar(20) NOT NULL,
  `buildingName` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(12) NOT NULL,
  `capacity` int(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `managementId` varchar(20) NOT NULL COMMENT 'NIK',
  `city` varchar(30) NOT NULL,
  `photoId` varchar(20) NOT NULL,
  `rate` float DEFAULT NULL,
  `price` int(10) NOT NULL,
  `bank` varchar(20) NOT NULL,
  `accountNumber` varchar(25) NOT NULL,
  `accountName` varchar(100) NOT NULL,
  `moreInfo` text,
  `latitude` varchar(50) DEFAULT NULL,
  `longtitude` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `building`
--

INSERT INTO `building` (`buildingId`, `buildingName`, `address`, `phone`, `capacity`, `status`, `managementId`, `city`, `photoId`, `rate`, `price`, `bank`, `accountNumber`, `accountName`, `moreInfo`, `latitude`, `longtitude`) VALUES
('SG_24042020010202', 'Winwin Party Bekasi', 'Jl. Harapan Baru Timur No.113, Kel, RT.007/RW.007, Kota Baru, Kec. Bekasi Bar., Kota Bks, Jawa Barat 17133', '02134567893', 100, 'N', '18011997', 'Bekasi', 'IMG_24042020010202', 4.2, 5000000, 'Mandiri', '14051424042020', 'Femuir', 'Banyak icon winnie the pooh nya loh', '-6.217355299999999', '106.9664744'),
('SG_25042020043858', 'Islamic Center Bekasi', 'Jl. Kemakmuran No.72, RT.004/RW.002, Marga Jaya, Kec. Bekasi Sel., Kota Bks, Jawa Barat 17141, Indonesia', '02188880000', 100, 'A', '22021996', 'Kota Bekasi', 'IMG_25042020043858', NULL, 4000000, 'mandiri', '22021990000006', 'Femuir', NULL, '-6.2459257', '106.9944465'),
('SG_25042020043936', 'Islamic Center Bekasi', 'Jl. Kemakmuran No.72, RT.004/RW.002, Marga Jaya, Kec. Bekasi Sel., Kota Bks, Jawa Barat 17141, Indonesia', '02188880000', 100, 'A', '22021996', 'Kota Bekasi', 'IMG_25042020043936', NULL, 4000000, 'mandiri', '22021990000006', 'Femuir', NULL, '-6.2459257', '106.9944465'),
('SG_25042020045643', 'Islamic Center Bekasi', 'Jl. Kemakmuran No.72, RT.004/RW.002, Marga Jaya, Kec. Bekasi Sel., Kota Bks, Jawa Barat 17141, Indonesia', '02188880000', 100, 'A', '22021996', 'Kota Bekasi', 'IMG_25042020045643', NULL, 4000000, 'mandiri', '22021990000006', 'Femuir', NULL, '-6.2459257', '106.9944465'),
('SG_25042020045714', 'Islamic Center Bekasi', 'Jl. Kemakmuran No.72, RT.004/RW.002, Marga Jaya, Kec. Bekasi Sel., Kota Bks, Jawa Barat 17141, Indonesia', '02188880000', 100, 'A', '22021996', 'Kota Bekasi', 'IMG_25042020045714', NULL, 4000000, 'mandiri', '22021990000006', 'Femuir', NULL, '-6.2459257', '106.9944465'),
('SG_25042020045737', 'Islamic Center Bekasi', 'Jl. Kemakmuran No.72, RT.004/RW.002, Marga Jaya, Kec. Bekasi Sel., Kota Bks, Jawa Barat 17141, Indonesia', '02188880000', 100, 'A', '22021996', 'Kota Bekasi', 'IMG_25042020045737', NULL, 4000000, 'mandiri', '22021990000006', 'Femuir', NULL, '-6.2459257', '106.9944465'),
('SG_25042020045756', 'Islamic Center Bekasi', 'Jl. Kemakmuran No.72, RT.004/RW.002, Marga Jaya, Kec. Bekasi Sel., Kota Bks, Jawa Barat 17141, Indonesia', '02188880000', 100, 'A', '22021996', 'Kota Bekasi', 'IMG_25042020045756', NULL, 4000000, 'mandiri', '22021990000006', 'Femuir', NULL, '-6.2459257', '106.9944465'),
('SG_25042020123117', 'Rhema Convention Hall', 'Ruko Datta Permai, Jl. KH. Noer Ali, RT.005/RW.006A, Jakasampurna, Kec. Bekasi Bar., Kota Bks, Jawa Barat 17145, Indonesia', '021368245179', 300, 'N', '2207199235000', 'Bekasi', 'IMG_25042020123117', 3.5714, 20000000, 'BRI 2', '90002765412', 'PT. Rhema Convention 2', 'Sebelum checkout check list booking dulu ya biar tanggalnya ga bentrok', '-6.248726899999999', '106.969924'),
('SG_27012020035019', 'Islamic Center Bekasi', 'Jl. Kemakmuran No.72, RT.004/RW.002, Marga Jaya, Kec. Bekasi Sel., Kota Bks, Jawa Barat 17141, Indonesia', '02188880000', 1000, 'N', '22021996', 'Kota Bekasi', 'IMG_27012020035019', 4, 6000000, 'BNI Syariah', '1234567890111213', 'Islamic Centre Bekasi', NULL, '-6.2459257', '106.9944465'),
('SG_31012020101143', 'Mustika Convention Hall Bekasi', 'Jl. Cipete Raya No.48, RT.005/RW.001, Mustikasari,…stika Jaya, Kota Bks, Jawa Barat 17157, Indonesia', '02188881111', 150, 'N', '22021996', 'Kota Bekasi', 'IMG_31012020101143', 4, 8000000, '', '99876543211987640', 'CV. Mustika Convention Hall', NULL, '-6.2850046', '107.0137589');

-- --------------------------------------------------------

--
-- Table structure for table `building_facility`
--

CREATE TABLE `building_facility` (
  `facilityId` int(11) NOT NULL,
  `buildingId` varchar(20) NOT NULL,
  `f1` varchar(50) DEFAULT NULL,
  `f2` varchar(50) DEFAULT NULL,
  `f3` varchar(50) DEFAULT NULL,
  `f4` varchar(50) DEFAULT NULL,
  `f5` varchar(50) DEFAULT NULL,
  `f6` varchar(50) DEFAULT NULL,
  `f7` varchar(50) DEFAULT NULL,
  `f8` varchar(50) DEFAULT NULL,
  `f9` varchar(50) DEFAULT NULL,
  `f10` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `building_facility`
--

INSERT INTO `building_facility` (`facilityId`, `buildingId`, `f1`, `f2`, `f3`, `f4`, `f5`, `f6`, `f7`, `f8`, `f9`, `f10`) VALUES
(19, 'SG_24042020010202', 'Parkiran lega', 'Ada panggung winnie the pooh', 'Ada temen2nya winnie', 'Ada madu winnie', NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'SG_31012020101143', 'panggung 2x3 m', 'ruang hias', 'listrik 5000watt', 'parkiran 10x10m', NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'SG_27012020035019', 'parkiran 20 x 20', 'ada masjid  yang luas untuk ijab kabul', 'ada asrama untuk sanak sodara', 'lokasi strategis, di tengah2 kota bekasi', NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'SG_25042020123117', 'Ruang rias 2', 'AC Standing 5 unit 2', 'Listrik 7000 watt 2', 'Parkir 2', 'Mushola 2', 'Channel WO yg berpengalaman', 'Channel cathering yg rasa dan harha bersahabat', 'Tets 2', 'Test 2', 'Test 2'),
(23, 'SG_25042020043858', 'akfla;kdfalk;afdkafd', 'adfgahgiquoeuq8r', '098893898', 'nmnmcm,njdsa', NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'SG_25042020043936', 'akfla;kdfalk;afdkafd', 'adfgahgiquoeuq8r', '098893898', 'nmnmcm,njdsa', NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'SG_25042020045643', 'akfla;kdfalk;afdkafd', 'adfgahgiquoeuq8r', '098893898', 'nmnmcm,njdsa', NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'SG_25042020045714', 'akfla;kdfalk;afdkafd', 'adfgahgiquoeuq8r', '098893898', 'nmnmcm,njdsa', NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'SG_25042020045737', 'akfla;kdfalk;afdkafd', 'adfgahgiquoeuq8r', '098893898', 'nmnmcm,njdsa', NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'SG_25042020045756', 'akfla;kdfalk;afdkafd', 'adfgahgiquoeuq8r', '098893898', 'nmnmcm,njdsa', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE `photo` (
  `photoId` varchar(20) NOT NULL,
  `profil` varchar(100) DEFAULT NULL,
  `pict1` varchar(100) DEFAULT NULL,
  `pict2` varchar(100) DEFAULT NULL,
  `pict3` varchar(100) DEFAULT NULL,
  `pict4` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photo`
--

INSERT INTO `photo` (`photoId`, `profil`, `pict1`, `pict2`, `pict3`, `pict4`) VALUES
('IMG_24042020010202', '1587708113838_IMG_24042020010202.jpg', '1587708142831_IMG_24042020010202.jpg', '1587708162470_IMG_24042020010202.jpg', '1587708188704_IMG_24042020010202.jpg', '1587708212705_IMG_24042020010202.jpg'),
('IMG_25042020043858', 'profil.jpg', '1.jpg', '2.jpg', '3.jpg', '4.jpg'),
('IMG_25042020043936', 'profil.jpg', '1.jpg', '2.jpg', '3.jpg', '4.jpg'),
('IMG_25042020045643', 'profil.jpg', '1.jpg', '2.jpg', '3.jpg', '4.jpg'),
('IMG_25042020045714', 'profil.jpg', '1.jpg', '2.jpg', '3.jpg', '4.jpg'),
('IMG_25042020045737', 'profil.jpg', '1.jpg', '2.jpg', '3.jpg', '4.jpg'),
('IMG_25042020045756', 'profil.jpg', '1.jpg', '2.jpg', '3.jpg', '4.jpg'),
('IMG_25042020123117', 'profil.jpg', '1588081677788_IMG_25042020123117.jpg', '2.jpg', '3.jpg', '4.jpg'),
('IMG_27012020035019', '1.jpg', '2.jpg', '3.jpg', '4.jpg', NULL),
('IMG_31012020101143', '1.jpg', '2.jpg', '3.JPG', '4.jpg', '5.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `rateId` int(11) NOT NULL,
  `buildingId` varchar(20) NOT NULL,
  `userId` varchar(20) NOT NULL,
  `transactionId` varchar(30) NOT NULL,
  `rateDate` date NOT NULL,
  `rate` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`rateId`, `buildingId`, `userId`, `transactionId`, `rateDate`, `rate`, `comment`) VALUES
(7, 'SG_25042020123117', '18011997', 'TRNSG30012020101859', '2020-03-20', 5, 'recomended gans'),
(8, 'SG_25042020123117', '18011997', 'TRNSG31012020101520', '2020-02-29', 4, 'mantaps gils gans'),
(9, 'SG_25042020123117', '22021996', 'TRNSG31012020101520', '2020-02-01', 4, 'mantaps gils gans'),
(15, 'SG_25042020123117', '22021996', 'TRNSG31012020101520', '2020-02-01', 4, 'mantaps gils gans'),
(17, 'SG_27012020035019', '22021996', 'BKGSG26042020101159', '2020-04-26', 4, 'good service, amazing lah pokoknya'),
(18, 'SG_25042020123117', '22021996', 'BKGSG27042020013236', '2020-04-26', 4, 'Amazing spiderman'),
(19, 'SG_25042020123117', '22021996', 'BKGSG26042020101433', '2020-04-29', 0, NULL),
(20, 'SG_25042020123117', '22021996', 'BKGSG26042020101433', '2020-04-29', 4, 'mangtaps jiwa gans');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `transactionId` varchar(30) NOT NULL,
  `buildingId` varchar(20) NOT NULL,
  `userId` varchar(20) NOT NULL COMMENT 'NIK',
  `date` date NOT NULL,
  `time` time NOT NULL,
  `proofOfPayment` varchar(100) DEFAULT NULL,
  `notes` varchar(150) DEFAULT NULL,
  `status` varchar(15) NOT NULL,
  `rated` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`transactionId`, `buildingId`, `userId`, `date`, `time`, `proofOfPayment`, `notes`, `status`, `rated`) VALUES
('BKGSG26042020101159', 'SG_25042020123117', '22021996', '2020-04-26', '09:00:00', NULL, 'yg di upload bukan bukti pembayaran', 'REJECT', 1),
('BKGSG26042020101251', 'SG_25042020123117', '22021996', '2020-04-26', '09:00:00', NULL, NULL, 'PAYMENT', 0),
('BKGSG26042020101433', 'SG_25042020123117', '22021996', '2020-08-26', '13:00:00', NULL, NULL, 'FINISH', 1),
('BKGSG27042020013236', 'SG_25042020123117', '22021996', '2020-05-14', '13:00:00', 'BKGSG27042020013236_1587926068720.jpg', 'Bismillah test', 'PAYMENT', 1),
('BKGSG29042020063142', 'SG_25042020043858', '22021996', '2020-10-28', '09:00:00', NULL, 'tracert', 'CHECKOUT', 0),
('BKGSG29042020063236', 'SG_25042020043858', '22021996', '2020-04-28', '09:00:00', NULL, NULL, 'CHECKOUT', 0),
('TRNSG26042020012341', 'SG_25042020123117', '22021996', '2020-04-25', '09:00:00', NULL, 'bukti transfer palsu!!', 'REJECT', 0),
('TRNSG26042020020201', 'SG_25042020123117', '22021996', '2020-04-25', '09:00:00', NULL, 'Bismillah halal', 'CHECKIN', 0),
('TRNSG26042020021914', 'SG_25042020123117', '22021996', '2020-07-25', '07:00:00', NULL, 'The last ya guys', 'PAYMENT', 0),
('TRNSG26042020120830', 'SG_25042020123117', '22021996', '2020-04-25', '09:00:00', NULL, 'test kaka', 'BOOKING', 0),
('TRNSG31012020101520', 'SG_25042020123117', '18011997', '2020-02-10', '09:30:00', 'proof of payment.png', NULL, 'FINISH', 0),
('TRNSG31012020101956', 'SG_25042020123117', '2207199235000', '2020-02-10', '09:30:00', 'proof of payment.png', NULL, 'FINISH', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` int(3) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updateAt` timestamp NULL DEFAULT NULL,
  `updateBy` varchar(100) DEFAULT NULL,
  `deleteAt` timestamp NULL DEFAULT NULL,
  `deleteBy` varchar(20) DEFAULT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `role`, `createdAt`, `updateAt`, `updateBy`, `deleteAt`, `deleteBy`, `status`) VALUES
(7, 'ferimirpan2@gmail.com', '$2y$10$uCi7iDF4IJKYnRX8WB1AvOHXxSjOMgcoqV08oKdogfX3FBdMTtoga', 2, '2020-04-25 06:32:27', '2020-03-28 17:15:33', 'ferimirpan2@gmail.com', NULL, NULL, 'A'),
(8, 'kapal@gmail.com', '$2y$10$CYzDzOFWzHtiPQQ3eNaWTu/F2MQa6aUw1MQpXe9AsPaEZCUXiwD0W', 2, '2020-03-25 10:22:04', NULL, NULL, NULL, NULL, 'A'),
(16, 'windut@gmail.com', '$2y$10$l7Vdu.kX/EaKvhkOtOntL.9ENOUAesk4xrnUvEoMaBk4Jq1098vvS', 1, '2020-04-24 05:55:57', NULL, NULL, NULL, NULL, 'A'),
(17, 'acew@gmail.com', '$2y$10$W2e6djRb.1HQugqJRHAFTeVsZSDoeTDCKozmnNOM21.BHApvkfThO', 1, '2020-04-24 17:26:33', NULL, NULL, NULL, NULL, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `user_detail`
--

CREATE TABLE `user_detail` (
  `userId` varchar(20) NOT NULL COMMENT 'NIK',
  `fullName` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `address` text NOT NULL,
  `picture` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_detail`
--

INSERT INTO `user_detail` (`userId`, `fullName`, `email`, `gender`, `phone`, `address`, `picture`) VALUES
('14052014', 'Kapal', 'kapal@gmail.com', 'male', '089612345678', 'Jl. Bintara 8 no. 2, Kecamatan Bekasi Barat. Kota Bekasi', NULL),
('18011997', 'Windut', 'windut@gmail.com', 'Female', '085711157332', 'Jl harapan baru 1', NULL),
('22021996', 'Feri Muhamad Irpan', 'ferimirpan2@gmail.com', 'Male', '08123456789', 'Jl. sadewa 10 No. 137 Bekasi Selatan Kota Bekasi', 'luffy.png'),
('2207199235000', 'Andri Muljono', 'acew@gmail.com', 'Male', '085712365478', 'Jl. Sadewa 10 No. 137', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `building`
--
ALTER TABLE `building`
  ADD PRIMARY KEY (`buildingId`);

--
-- Indexes for table `building_facility`
--
ALTER TABLE `building_facility`
  ADD PRIMARY KEY (`facilityId`);

--
-- Indexes for table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`photoId`);

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`rateId`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`transactionId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `user_detail`
--
ALTER TABLE `user_detail`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `building_facility`
--
ALTER TABLE `building_facility`
  MODIFY `facilityId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `rateId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
