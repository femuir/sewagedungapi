<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {
    Route::post('register', 'User\UserController@registerUser');
    Route::post('login', 'User\UserController@loginUser');
    Route::post('building/getListBuilding', 'Building\BuildingController@listBuilding');
    Route::get('building/detailBuilding/{id}', 'Building\BuildingController@detailBuilding');
    Route::post('building/inputBuilding', 'Building\BuildingController@inputBuilding');
    Route::post('building/updateBuilding', 'Building\BuildingController@updateBuilding');
    Route::post('building/updatePhoto', 'Building\BuildingController@updatePhoto');
    Route::post('user/updateProfil', 'User\UserController@updateProfil');
    Route::post('user/getProfil', 'User\UserController@getProfil');
    Route::post('user/updatePhoto', 'User\UserController@updatePhotoProfil');
    Route::post('user/resetPassword', 'User\UserController@resetPassword');
    Route::post('transaction/checkout', 'Transaction\TransactionController@inputTrx');
    Route::post('transaction/uploadPayment', 'Transaction\TransactionController@uploadPayment');
    Route::post('transaction/updateTrx', 'Transaction\TransactionController@updateTrx');
    Route::post('transaction/listTrx', 'Transaction\TransactionController@listTrx');
    Route::post('transaction/detailTrx', 'Transaction\TransactionController@detailTrx');
    Route::post('transaction/inputRate', 'Transaction\TransactionController@inputRate');
    Route::post('transaction/listRate', 'Transaction\TransactionController@listRate');
    Route::get('notification/getNotification/{to}', 'Notification\NotificationController@getNotification');
    Route::get('notification/updateNotification/{to}', 'Notification\NotificationController@updateNotification');
});