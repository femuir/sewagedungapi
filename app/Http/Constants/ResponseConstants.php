<?php

namespace App\Http\Constants;

class ResponseConstants
{
    const ERROR = array('status' => 500, 'msg' => 'Terjadi kesalahan pada sistem, silahkan hubungi administrator.');
    const SUCCESS = array('status' => 200, 'msg' => 'Sukses mendapatkan informasi akun.');

    const REGISTRATION_SUCCESS = array('status' => 200, 'msg' => 'Registrasi berhasil.');
    const REGISTRATION_EMAIL_ALREADY_EXISTS = array('status' => 400, 'msg' => 'Email sudah terdaftar, silahkan masukan email lain.');
    const REGISTRATION_NIK_ALREADY_EXISTS = array('status' => 400, 'msg' => 'maaf NIK anda sudah terdaftar, silahkan cek kembali.');
    const REGISTRATION_PHONE_LIMIT = array('status' => 400, 'msg' => 'maaf nomor HP anda melebihi jumlah maximal karakter, max 12 digit.');

    const REGISTRATION_BUILDING_SUCCESS = array('status' => 200, 'msg' => 'Registrasi Gedung berhasil, silahkan lakukan login terlebih dahulu.');
    const BUILDING_ID_NOT_EXIST = array('status' => 400, 'msg' => 'Maaf Id Gedung tidak ditemukan.');
    const UPDATE_BUILDING_SUCCESS = array('status' => 200, 'msg' => 'Update Gedung berhasil.');
    const PHOTO_ID_NOT_EXIST = array('status' => 400, 'msg' => 'Maaf Id foto tidak ditemukan.');

    const LOGIN_SUCCESS = array('status' => 200, 'msg' => 'Login berhasil.');
    const LOGIN_INVALID = array('status' => 400, 'msg' => 'Maaf email atau kata sandi yang anda masukan salah.');

    const RESET_REQUEST_SUCCESS = array('status' => 200, 'msg' => 'Link reset kata sandi berhasil dikirim ke alamat email anda.');
    const RESET_PASSWORD_SUCCESS = array('status' => 200, 'msg' => 'Ubah kata sandi berhasil.');

    const UPDATE_USER_SUCCESS = array('status' => 200, 'msg' => 'Update profil berhasil.');
    const USER_ID_NOT_EXIST = array('status' => 400, 'msg' => 'Maaf Id user tidak ditemukan.');
    const USER_EMAIL_NOT_EXISTS = array('status' => 400, 'msg' => 'Maaf email tidak ditemukan.');
    const USER_INVALID_PASSWORD = array('status' => 400, 'msg' => 'Maaf kata sandi yang anda masukan salah.');
    const USER_PASSWORD_NOT_MATCH = array('status' => 400, 'msg' => 'Maaf kata sandi anda tidak sesuai dengan konfirmasi kata sandi anda, silahkan cek kembali.');

    const CHECKOUT_TRX_SUCCESS = array('status' => 200, 'msg' => 'Checkout gedung berhasil.');
    const UPLOAD_PAYMENT_SUCCESS = array('status' => 200, 'msg' => 'Upload bukti pembayaran berhasil, silahkan menunggu verifikasi admin.');
    const TRN_ID_IS_NOT_EXIST = array('status' => 400, 'msg' => 'Maaf ID transaksi tidak ditemukan.');
    const BOOKING_TIME_IS_EXIST = array('status' => 400, 'msg' => 'Maaf jadwal yang anda input sudah di Booking, silahkan cek Jadwal Booking di Menu List Booking.');
    const UPDATE_TRX_SUCCESS = array('status' => 200, 'msg' => 'Transaksi berhasil diupdate.');
    const INPUT_RATE_SUCCESS = array('status' => 200, 'msg' => 'Terima kasih telah memberikan penilain dan komentar.');

    const UPDATE_NOTIFICATION = array('status' => 200, 'msg' => 'Update Notification berhasil.');
    const NOTIFICATION_ID_NOT_EXIST = array('status' => 400, 'msg' => 'Maaf Notifikasi ID tidak ditemukan.');
    
}