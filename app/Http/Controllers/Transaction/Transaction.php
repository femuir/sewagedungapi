<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Constants\ResponseConstants;
use App\Http\Controllers\Notification\Notification;
use App\Http\Utils\ResponseException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\User\User;
use App\Http\Controllers\Building\Building;

class Transaction
{
    public function __construct()
    {}

    public function getListTrx($request)
    {
        try
        {
            // get listTransaction
            $transaction = $this->doGetListTrx($request);

            return $transaction;

        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception($e);
        }
    }

    public function getDetailTrx($request)
    {
        if ($this->doCheckTrnId($request->trnId) == null) {
            //validation transaction id is not exist
            throw new ResponseException(ResponseConstants::TRN_ID_IS_NOT_EXIST);
        } else {
            try
            {
                // get data transaction
                $detailTrx = $this->doGetDetailTrx($request);

                //get data building
                $building = DB::table('building')
                    ->select('building.*', 'photo.profil')
                    ->leftJoin('photo', 'photo.photoId', '=', 'building.photoId')
                    ->where('building.buildingId', '=', $request->buildingId)
                    ->first();

                $data = array(
                    'detailTrx' => $detailTrx,
                    'building' => $building,
                );

                return $data;
            } catch (Exception $e) {
                DB::rollBack();
                throw new Exception($e);
            }

        }
    }

    public function InputTrx($request)
    {
        date_default_timezone_set("Asia/Bangkok");
        $transactionId = 'BKGSG' . date("Ymdhis");
        $descTime = '';
        $user = new User();
        $building = new Building();
        
        try {
            $checkUser = $user->doCheckUser($request->userId);
            $checkBuilding = $building->doCheckBuildingId($request->buildingId);
            if ($request->time <= '14:00:00') {
                $descTime = 'AM';
            } else {
                $descTime = 'PM';
            }
            if(!$checkUser) {
                //validation user id is not exist
                throw new ResponseException(ResponseConstants::USER_ID_NOT_EXIST);
            } else if (!$checkBuilding) {
                //validation building id is not exist
                throw new ResponseException(ResponseConstants::BUILDING_ID_NOT_EXIST);
            } else if ($this->doValidateBookingTIme($request, $descTime)) {
                //validation booking time is exist
                throw new ResponseException(ResponseConstants::BOOKING_TIME_IS_EXIST);
            }
                DB::beginTransaction();

                // insert into table transaction
                $this->doInsertTransaction($request, $transactionId, $descTime);

                DB::commit();

                return $transactionId;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception($e);
        }
    }

    public function uploadProofOfPayment($request)
    {
        $notification = new Notification();
        $checkTrx = $this->doCheckTrnId($request->trnId);

        if ($checkTrx == null) {
            //validation transaction id is not exist
            throw new ResponseException(ResponseConstants::TRN_ID_IS_NOT_EXIST);
        } else {
            try
            {
                DB::beginTransaction();

                // update table transaction
                $this->doUpdateTrx($request);
                // update photo
                $this->doUploadTrx($request);
                //insert notification
                $notification->inputNotification($checkTrx, $request->status, $request->updatedBy);

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                throw new Exception($e);
            }
        }
    }

    public function updateTrx($request)
    {
        $notification = new Notification();
        $checkTrx = $this->doCheckTrnId($request->trnId);

        if ($checkTrx == null) {
            //validation building id is not exist
            throw new ResponseException(ResponseConstants::TRN_ID_IS_NOT_EXIST);
        } else {
            try
            {
                DB::beginTransaction();

                // update table transaction
                $this->doUpdateTrx($request);
                //insert notification
                $notification->inputNotification($checkTrx, $request->status, $request->updatedBy);

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                throw new Exception($e);
            }
        }
    }

    public function inputRate($request)
    {
        if ($this->doCheckTrnId($request->trnId) == null) {
            //validation building id is not exist
            throw new ResponseException(ResponseConstants::TRN_ID_IS_NOT_EXIST);
        } else {
            try
            {
                DB::beginTransaction();

                // insert table rate
                $this->doInsertRate($request);
                // update rate in table building
                $this->doUpdateRate($request);
                // update table transaction
                $this->doUpdateTrx($request);

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                throw new Exception($e);
            }
        }
    }

    public function getRate($request)
    {
        try
        {
            // get Rate
            $rate = $this->doGetListRate($request);

            return $rate;

        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception($e);
        }
    }

    private function doInsertTransaction($trx, $id, $descTime)
    {
        DB::table('transaction')->insert([
            'transactionId' => $id,
            'buildingId' => $trx->buildingId,
            'userId' => $trx->userId,
            'date' => $trx->date,
            'time' => $trx->time,
            'descTime' => $descTime,
            'notes' => $trx->notes,
            'status' => 'CHECKOUT',
        ]);
    }

    private function doCheckTrnId($id)
    {
        $trnId = DB::table('transaction')->where("transactionId", '=', $id)->first();
        return $trnId;
    }

    private function doUpdateTrx($trx)
    {
        $data = [];

        if (!empty($trx->input('date'))) {
            $data['date'] = $trx->input('date');
        }

        if (!empty($trx->input('time'))) {
            $data['time'] = $trx->input('time');
        }

        if (!empty($trx->file('payment'))) {
            $data['proofOfPayment'] = $trx->file('payment')->getClientOriginalName();
        }

        if (!empty($trx->input('notes'))) {
            $data['notes'] = $trx->input('notes');
        }

        if (!empty($trx->input('status'))) {
            $data['status'] = $trx->input('status');
        }

        if (!empty($trx->input('rated'))) {
            $data['rated'] = $trx->input('rated');
        }

        DB::table('transaction')->where('transactionId', $trx->trnId)->update($data);
    }

    private function doUploadTrx($trx)
    {
        //inisiate variable
        $path = 'Transaction/' . $trx->trnId;
        $payment = $trx->file('payment');

        //doupload
        if ($payment != null) {
            $payment->move($path, $payment->getClientOriginalName());
        }
    }

    private function doGetListTrx($request)
    {

        $page = $request->input('page') ? $request->input('page') : 1;
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $userId = $request->input('userId');
        $role = $request->input('role');
        $buildingId = $request->input('buildingId');
        $status1 = $request->input('status1');
        $status2 = $request->input('status2');
        $status3 = $request->input('status3');
        $status4 = $request->input('status4');
        $date = $request->input('date');

        $skip = ($page == 1) ? $page - 1 : (($page - 1) * $limit);
        $query = DB::table('transaction')
            ->select('transaction.*', 'building.buildingName', 'building.address', 'building.photoId', 'photo.profil', 'building.price', 'user_detail.*')
            ->leftJoin('building', 'building.buildingId', '=', 'transaction.buildingId')
            ->leftJoin('user_detail', 'user_detail.userId', '=', 'transaction.userId')
            ->leftJoin('photo', 'photo.photoId', '=', 'building.photoId');

        if ($userId) {
            $query = $query->where('transaction.userId', '=', $userId);
        }

        if ($buildingId) {
            $query = $query->where('transaction.buildingId', '=', $buildingId);
        }

        if ($status1 || $status2 || $status3 || $status4) {
            $query = $query->whereIn('transaction.status', [$status1, $status2, $status3, $status4]);
        }

        if ($date) {
            $query = $query->where('transaction.date', '=', $date);
        }

        if ($role == 2) {
            $query = $query->orderBy('date', 'desc');
        } else {
            $query = $query->orderBy('date', 'asc');
        }

        $query->skip($skip);
        $query->limit($limit);

        $total = $query->count();
        $totalPage = ceil($total / $limit);

        $query = $query->get();

        $data = array(
            'data' => $query,
            'limit' => (int) $limit,
            'page' => (int) $page,
            'total' => $total,
            'totalPage' => $totalPage,
        );

        return $data;
    }

    private function doGetListRate($request)
    {
        $page = $request->input('page') ? $request->input('page') : 1;
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $buildingId = $request->input('buildingId');

        // print_r($buildingId); exit();
        $skip = ($page == 1) ? $page - 1 : (($page - 1) * $limit);
        $query = DB::table('rate')
            ->select('rate.*', 'user_detail.fullName', 'user_detail.picture')
            ->leftJoin('user_detail', 'user_detail.userId', '=', 'rate.userId');

        if ($buildingId) {
            $query = $query->where('rate.buildingId', '=', $buildingId);
        }

        $query->skip($skip);
        $query->limit($limit);

        $total = $query->count();
        $totalPage = ceil($total / $limit);

        $query = $query->orderBy('rateDate', 'desc')->get();
        // $getRate = DB::table('rate')->where('buildingId', $buildingId)->avg('rate');
        // $rate = number_format($getRate, 2);

        $data = array(
            'data' => $query,
            'limit' => (int) $limit,
            'page' => (int) $page,
            'total' => $total,
            'totalPage' => $totalPage,
        );

        return $data;
    }

    private function doGetDetailTrx($request)
    {
        $query = DB::table('transaction')->where('transactionId', '=', $request->trnId)->first();

        return $query;
    }

    private function doInsertRate($rate)
    {
        $date = Carbon::now()->toDateString();

        DB::table('rate')->insert([
            'buildingId' => $rate->buildingId,
            'userId' => $rate->userId,
            'transactionId' => $rate->trnId,
            'rateDate' => $date,
            'rate' => $rate->rate,
            'comment' => $rate->comment,
        ]);
    }

    private function doUpdateRate($rate)
    {
        $bId = $rate->buildingId;
        $rate = DB::table('rate')->where('buildingId', $bId)->avg('rate');

        DB::table('building')->where('buildingId', $bId)->update([
            'rate' => $rate,
        ]);

    }

    private function doValidateBookingTIme($booking, $descTime)
    {
        $bId = $booking->buildingId;
        $bookingDate = $booking->date;
        $booking = DB::table('transaction')
            ->where('buildingId', $bId)
            ->where('date', $bookingDate)
            ->where('descTime', $descTime)->whereNotIn('status', ['REJECT'])->first();

        return $booking;
    }
}
