<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Constants\ResponseConstants;
use App\Http\Controllers\Controller;
use App\Http\Utils\ResponseException;
use Illuminate\Http\Request;
use Throwable;

class TransactionController extends Controller
{
    public function __construct()
    { }

    public function listTrx(Request $request)
    {
        $status = [];
        $transaction = [];

        try {
            $transactionService = new Transaction();
            $transaction = $transactionService->getListTrx($request);

            $status = ResponseConstants::SUCCESS;
        } catch (ResponseException $th) {
            $status = $th->getResponse();
        } catch (Throwable $th) {
            $status = ResponseConstants::ERROR;
            $status['error_msg'] = $th->getMessage();
            $status['stactrace'] = $th->getTraceAsString();
        }

        if ($status['status'] == 500) {
            $data = array(
                'msg' => $status['msg'],
                'error_msg' => $status['error_msg'],
                'stactrace' => $status['stactrace']
            );
        } else {
            $data = array(
                'msg' => $status['msg'],
                'transaction' => $transaction,
            );
        }

        return response()->json($data, $status['status']);
    }

    public function detailTrx(Request $request)
    {
        $status = [];
        $transaction = [];

        try {
            $transactionService = new Transaction();
            $transaction = $transactionService->getDetailTrx($request);

            $status = ResponseConstants::SUCCESS;
        } catch (ResponseException $th) {
            $status = $th->getResponse();
        } catch (Throwable $th) {
            $status = ResponseConstants::ERROR;
            $status['error_msg'] = $th->getMessage();
            $status['stactrace'] = $th->getTraceAsString();
        }

        if ($status['status'] == 500) {
            $data = array(
                'msg' => $status['msg'],
                'error_msg' => $status['error_msg'],
                'stactrace' => $status['stactrace']
            );
        } else {
            $data = array(
                'msg' => $status['msg'],
                'transaction' => $transaction,
            );
        }

        return response()->json($data, $status['status']);
    }

    public function inputTrx(Request $request)
    {
        $transaction = [];
        $status = [];
        
        try {
            $transactionService = new Transaction();
            $transaction = $transactionService->InputTrx($request);

            $status = ResponseConstants::CHECKOUT_TRX_SUCCESS;
        } catch (ResponseException $th) {
            $status = $th->getResponse();
        } catch (Throwable $th) {
            $status = ResponseConstants::ERROR;
            $status['error_msg'] = $th->getMessage();
            $status['stactrace'] = $th->getTraceAsString();
        }

        if ($status['status'] == 500) {
            $data = array(
                'msg' => $status['msg'],
                'error_msg' => $status['error_msg'],
                'stactrace' => $status['stactrace']
            );
        } else {
            $data = array(
                'msg' => $status['msg'],
                'transaction' => $transaction,
            );
        }

        return response()->json($data, $status['status']);
    }

    public function uploadPayment(Request $request)
    {
        $data = [];
        
        try {
            $transactionService = new Transaction();
            $transactionService->uploadProofOfPayment($request);

            $data = ResponseConstants::UPLOAD_PAYMENT_SUCCESS;
        } catch (ResponseException $th) {
            $data = $th->getResponse();
        } catch (Throwable $th) {
            $data = ResponseConstants::ERROR;
            $data['error_msg'] = $th->getMessage();
            $data['stactrace'] = $th->getTraceAsString();
        }

        return response()->json($data, $data['status']);
    }

    public function updateTrx(Request $request)
    {
        $data = [];
        
        try {
            $transactionService = new Transaction();
            $transactionService->updateTrx($request);

            $data = ResponseConstants::UPDATE_TRX_SUCCESS;
        } catch (ResponseException $th) {
            $data = $th->getResponse();
        } catch (Throwable $th) {
            $data = ResponseConstants::ERROR;
            $data['error_msg'] = $th->getMessage();
            $data['stactrace'] = $th->getTraceAsString();
        }

        return response()->json($data, $data['status']);
    }

    public function inputRate(Request $request)
    {
        $data = [];
        
        try {
            $transactionService = new Transaction();
            $transactionService->inputRate($request);

            $data = ResponseConstants::INPUT_RATE_SUCCESS;
        } catch (ResponseException $th) {
            $data = $th->getResponse();
        } catch (Throwable $th) {
            $data = ResponseConstants::ERROR;
            $data['error_msg'] = $th->getMessage();
            $data['stactrace'] = $th->getTraceAsString();
        }

        return response()->json($data, $data['status']);
    }

    public function listRate(Request $request)
    {
        $status = [];
        $rate = [];
        
        try {
            $transactionService = new Transaction();
            $rate = $transactionService->getRate($request);

            $status = ResponseConstants::SUCCESS;
        } catch (ResponseException $th) {
            $status = $th->getResponse();
        } catch (Throwable $th) {
            $status = ResponseConstants::ERROR;
            $status['error_msg'] = $th->getMessage();
            $status['stactrace'] = $th->getTraceAsString();
        }

        if ($status['status'] == 200) {
            $data = array(
                'msg' => $status['msg'],
                'rate' => $rate,
            );
        } else {
            $data = array(
                'msg' => $status['msg'],
                'rate' => $rate,
                'error_msg' => $status['error_msg'],
                'stactrace' => $status['stactrace']
            );
        }
        
        return response()->json($data, $status['status']);
    }
}
