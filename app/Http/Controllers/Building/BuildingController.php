<?php

namespace App\Http\Controllers\Building;

use App\Http\Constants\ResponseConstants;
use App\Http\Controllers\Controller;
use App\Http\Utils\ResponseException;
use Illuminate\Http\Request;
use Throwable;

class BuildingController extends Controller
{
    public function __construct()
    { }

    public function listBuilding(Request $request)
    {
        $status = [];
        $building = [];

        try {
            $buildingService = new Building();
            $building = $buildingService->getListBuilding($request);

            $status = ResponseConstants::SUCCESS;
        } catch (ResponseException $th) {
            $status = $th->getResponse();
        } catch (Throwable $th) {
            $status = ResponseConstants::ERROR;
            $status['error_msg'] = $th->getMessage();
            $status['stactrace'] = $th->getTraceAsString();
        }

        if ($status['status'] == 200){
            $data = array(
                'msg' => $status['msg'],
                'building' => $building
            );
        } else {
            $data = array(
                'msg' => $status['msg'],
                'error_msg' => $status['error_msg'],
                'track_error' => $status['stactrace']
            );
        }

        return response()->json($data, $status['status']);
    }

    public function detailBuilding($id)
    {
        $status = [];
        $building = [];

        try {
            $buildingService = new Building();
            $building = $buildingService->getDetailBuilding($id);

            $status = ResponseConstants::SUCCESS;
        } catch (ResponseException $th) {
            $status = $th->getResponse();
        } catch (Throwable $th) {
            $status = ResponseConstants::ERROR;
            $status['error_msg'] = $th->getMessage();
            $status['stactrace'] = $th->getTraceAsString();
        }

        $data = array(
            'msg' => $status['msg'],
            'building' => $building,
        );

        return response()->json($data, $status['status']);
    }

    public function inputBuilding(Request $request)
    {
        // $data = [];
        $status = [];
        $building = [];
        
        try {
            $buildingService = new Building();
            $building = $buildingService->doInputBuilding($request);

            // $data = ResponseConstants::REGISTRATION_BUILDING_SUCCESS;
            $status = ResponseConstants::REGISTRATION_BUILDING_SUCCESS;
        } catch (ResponseException $th) {
            $status = $th->getResponse();
        } catch (Throwable $th) {
            $status = ResponseConstants::ERROR;
            $status['error_msg'] = $th->getMessage();
            $status['stactrace'] = $th->getTraceAsString();
        }

        if ($status['status'] == 200){
            $data = array(
                'msg' => $status['msg'],
                'buildingId' => $building['buildingId'],
                'photoId' => $building['photoId']
            );
        } else {
            $data = array(
                'msg' => $status['msg'],
                'error_msg' => $status['error_msg'],
                'track_error' => $status['stactrace']
            );
        }

        return response()->json($data, $status['status']);
    }

    public function updateBuilding(Request $request)
    {
        $data = [];
        
        try {
            $buildingService = new Building();
            $buildingService->updateBuilding($request);

            $data = ResponseConstants::UPDATE_BUILDING_SUCCESS;
        } catch (ResponseException $th) {
            $data = $th->getResponse();
        } catch (Throwable $th) {
            $data = ResponseConstants::ERROR;
            $data['error_msg'] = $th->getMessage();
            $data['stactrace'] = $th->getTraceAsString();
        }

        return response()->json($data, $data['status']);
    }

    public function updatePhoto(Request $request)
    {
        $data = [];
        
        try {
            $buildingService = new Building();
            $buildingService->updatePhoto($request);

            $data = ResponseConstants::UPDATE_BUILDING_SUCCESS;
        } catch (ResponseException $th) {
            $data = $th->getResponse();
        } catch (Throwable $th) {
            $data = ResponseConstants::ERROR;
            $data['error_msg'] = $th->getMessage();
            $data['stactrace'] = $th->getTraceAsString();
        }

        return response()->json($data, $data['status']);
    }
}
