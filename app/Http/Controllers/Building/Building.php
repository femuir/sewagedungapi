<?php

namespace App\Http\Controllers\Building;

use App\Http\Constants\ResponseConstants;
use App\Http\Utils\ResponseException;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Building
{
    public function __construct()
    { }

    public function getListBuilding($request)
    {
        try 
        {
            // get listBuilding
            $building = $this->doGetListBuilding($request);

            return $building;
            
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception($e);
        }
    }

    public function getDetailBuilding($id)
    {
        try 
        {
            if ($this->doCheckBuildingId($id) == null)
            {
                //validation building id is not exist
                throw new ResponseException(ResponseConstants::BUILDING_ID_NOT_EXIST);
            } else {
                // get detailBuilding
                $query = DB::table('building');
                $query->select('building.*', 'photo.*', 'building_facility.*');
                $query->leftjoin('photo', 'photo.photoId', '=', 'building.photoId');
                $query->leftjoin('building_facility', 'building_facility.buildingId', '=', 'building.buildingId');
                $query->where('building.buildingId', $id);
                $query = $query->first();

                // $data = array(
                //     'detail' =>  $query,
                //     'review' => 'test review',
                //     'listBooking' => 'testListBooking'
                // );

                return $query;
            }
            
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception($e);
        }   
    }

    public function doInputBuilding($request)
    {
        date_default_timezone_set("Asia/Bangkok");
        $buildingId = 'SG_'.date("Ymdhis");
        $photoId = 'IMG_'.date("Ymdhis");

        try 
        {
            DB::beginTransaction();

            // insert into table building
            $this->doInsertBuilding($request, $buildingId, $photoId);
            // insert into table building_facility
            $this->doInsertFacility($request, $buildingId);
            // insert into table photo
            DB::table('photo')->insert([
                'photoId' => $photoId,
            ]);

            DB::commit();

            $resBuilding = array(
                'buildingId' => $buildingId,
                'photoId' => $photoId
            );

            return $resBuilding;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception($e);
        }
    }

    public function updateBuilding($request)
    {
        if ($this->doCheckBuildingId($request->buildingId) == null)
        {
            //validation building id is not exist
            throw new ResponseException(ResponseConstants::BUILDING_ID_NOT_EXIST);
        } else {
            try 
            {
                DB::beginTransaction();

                // update table building
                $this->doUpdateBuilding($request);
                //update table building_facility
                $this->doUpdateBuildingFacility($request);

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                throw new Exception($e);
            }
        }
    }

    public function updatePhoto($request)
    {
        if ($this->doCheckPhotoId($request->photoId) == null)
        {
            //validation building id is not exist
            throw new ResponseException(ResponseConstants::PHOTO_ID_NOT_EXIST);
        } else {
            try 
            {
                DB::beginTransaction();

                // update table photo
                $this->doUpdatePhoto($request);
                // update photo
                $this->doUploadPhoto($request, $request->photoId);

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                throw new Exception($e);
            }
        }
    }

    public function doCheckBuildingId($id)
    {
        $IdIsExist = DB::table('building')->where("buildingId", '=', $id)->first();
        return $IdIsExist;
    }

    private function doCheckPhotoId($id)
    {
        $IdIsExist = DB::table('photo')->where("photoId", '=', $id)->first();
        return $IdIsExist;
    }

    private function doGetListBuilding($request)
    {
        $page = $request->input('page') ? $request->input('page') : 1;
        $limit = $request->input('limit') ? $request->input('limit') : 10;
        $search = $request->input('search');
        $sortBy = $request->input('sortBy');
        $price = $request->input('price');
        $rating = $request->input('rating');
        $capacity = $request->input('capacity');
        $radian = 0.0174532925;
        $lat1 = $request->input('lat1') * $radian;
        $long1 = $request->input('long1') * $radian;
        

        $skip = ($page == 1) ? $page - 1 : (($page - 1) * $limit);
        $query = DB::table('building')
            ->select('building.*', 'photo.profil')
            //begin, haversine formula
            ->selectRaw('ROUND(6371 * SQRT(
                (((building.longtitude * '.$radian.') - '.$long1.') * cos((building.latitude + '.$lat1.') / 2)) *
                (((building.longtitude * '.$radian.') - '.$long1.') * cos((building.latitude + '.$lat1.') / 2)) + 
                ((building.latitude * '.$radian.') - '.$lat1.') * 
                ((building.latitude * '.$radian.') - '.$lat1.')
            ), 1) AS distance')
            //end, haversine formula
            ->leftJoin('photo', 'photo.photoId', '=', 'building.photoId');

        if ($search) {
            $query = $query->where('buildingName', 'like', '%' . $search . '%');
        }

        if ($price && $price > 500000) {

            $query = $query->where('price', '<=', $price);
        }

        if ($rating > 1) {
            $query = $query->where('rate', '<=', $rating);
        }

        if ($capacity > 100) {
            $query = $query->where('capacity', '<=', $capacity);
        }

        if ($sortBy == 'lowPrice') {
            $query = $query->orderBy('price', 'asc');
        } else if ($sortBy == 'highPrice') {
            $query = $query->orderBy('price', 'desc');
        } else if ($sortBy == 'rating') {
            $query = $query->orderBy('rate', 'desc');
        } else if ($sortBy == 'distance') {
            $query = $query->orderBy('distance', 'asc');
        }

        $query->skip($skip);
        $query->limit($limit);

        $total = $query->count();
        $totalPage = ceil($total / $limit);

        $query = $query->get();

        $data = array(
            'data' => $query,
            'limit' => (int) $limit,
            'page' => (int) $page,
            'total' => $total,
            'totalPage' => $totalPage,
        );

        return $data;
    }

    private function doInsertBuilding($building, $buildingId, $photoId)
    {
        DB::table('building')->insert([
            'buildingId' => $buildingId,
            'buildingName' => $building->buildingName,
            'address' => $building->address,
            'phone' => $building->phone,
            'capacity' => $building->capacity,
            'status' => $building->status,
            'managementId' => $building->managementId,
            'city' => $building->city,
            'photoId' => $photoId,
            'rate' => null,
            'price' => $building->price,
            'bank' => $building->bank,
            'accountNumber' => $building->accountNumber,
            'accountName' => $building->accountName,
            'moreInfo' => $building->moreInfo,
            'latitude' => $building->latitude,
            'longtitude' => $building->longtitude
        ]);
    }

    private function doInsertFacility($facility, $buildingId)
    {
        DB::table('building_facility')->insert([
            'buildingId' => $buildingId,
            'f1' => $facility->f1,
            'f2' => $facility->f2,
            'f3' => $facility->f3,
            'f4' => $facility->f4,
            'f5' => $facility->f5,
            'f6' => $facility->f6,
            'f7' => $facility->f7,
            'f8' => $facility->f8,
            'f9' => $facility->f9,
            'f10' => $facility->f10,
            
        ]);
    }


    private function doUpdateBuilding($building)
    {
        DB::table('building')->where('buildingId', $building->buildingId)
        ->update([
            'buildingName' => $building->buildingName,
            'address' => $building->address,
            'phone' => $building->phone,
            'capacity' => $building->capacity,
            'price' => $building->price,
            'moreInfo' => $building->moreInfo,
            'status' => $building->status,
            'managementId' => $building->managementId,
            'city' => $building->city,
            'bank' => $building->bank,
            'accountNumber' => $building->accountNumber,
            'accountName' => $building->accountName,
            'latitude' => $building->latitude,
            'longtitude' => $building->longtitude
        ]);
    }

    private function doUpdateBuildingFacility($building)
    {
        DB::table('building_facility')->where('buildingId', $building->buildingId)
        ->update([
            'f1' => $building->f1,
            'f2' => $building->f2,
            'f3' => $building->f3,
            'f4' => $building->f4,
            'f5' => $building->f5,
            'f6' => $building->f6,
            'f7' => $building->f7,
            'f8' => $building->f8,
            'f9' => $building->f9,
            'f10' => $building->f10,
        ]);
    }

    private function doUpdatePhoto($building)
    {
        $data = [];

        if (!empty($building->file('profil')))
            $data['profil'] = $building->file('profil')->getClientOriginalName();
        if (!empty($building->file('pict1')))
            $data['pict1'] = $building->file('pict1')->getClientOriginalName();
        if (!empty($building->file('pict2')))
            $data['pict2'] = $building->file('pict2')->getClientOriginalName();
        if (!empty($building->file('pict3')))
            $data['pict3'] = $building->file('pict3')->getClientOriginalName();
        if (!empty($building->file('pict4')))
            $data['pict4'] = $building->file('pict4')->getClientOriginalName();

        DB::table('photo')->where('photoId', $building->photoId)->update($data);
    }

    private function doUploadPhoto($building, $photoId)
    {
        //inisiate variable
        $path = 'Building/' . $photoId;
        $profil = $building->file('profil');
        $pict1 = $building->file('pict1');
        $pict2 = $building->file('pict2');
        $pict3 = $building->file('pict3');
        $pict4 = $building->file('pict4');

        //doupload
        if ($profil != null) {
            $profil->move($path, $profil->getClientOriginalName());
        }

        if ($pict1 != null) {
            $pict1->move($path, $pict1->getClientOriginalName());
        }

        if ($pict2 != null) {
            $pict2->move($path, $pict2->getClientOriginalName());
        }

        if ($pict3 != null) {
            $pict3->move($path, $pict3->getClientOriginalName());
        }

        if ($pict4 != null) {
            $pict4->move($path, $pict4->getClientOriginalName());
        } 
    }
}