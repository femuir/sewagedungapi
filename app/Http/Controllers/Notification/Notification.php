<?php

namespace App\Http\Controllers\Notification;

use App\Http\Constants\ResponseConstants;
use App\Http\Utils\ResponseException;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Building\Building;
use App\Http\Controllers\User\User;

class Notification
{
    public function __construct()
    {}

    public function inputNotification($trx, $status, $updatedBy)
    {
        $from = '';
        $to = '';
        $title = '';
        $desc = '';
        $isTransaction = null;
        $transactionId = '';
        $buildingId = '';
        $building = new Building();
        $user = new User();
        $checkBuilding = $building->doCheckBuildingId($trx->buildingId);
        $checkUser = $user->doCheckUser($trx->userId);

        try {
            if (!$checkUser) {
                //validation user id is not exist
                throw new ResponseException(ResponseConstants::USER_ID_NOT_EXIST);
            } else if (!$checkBuilding) {
                //validation building id is not exist
                throw new ResponseException(ResponseConstants::BUILDING_ID_NOT_EXIST);
            } else if ($updatedBy == 'USER' && $status == 'DIBAYAR') {
                //user upload proof of payment
                $from = $trx->userId;
                $to = $checkBuilding->managementId;
                $title = 'Transaksi Baru';
                $desc = 'Bukti pembayaran sudah di upload, silahkan lakukan verifikasi';
                $isTransaction = 1;
                $transactionId = $trx->transactionId;
                $buildingId = $trx->buildingId;
            } else if ($updatedBy == 'USER' && $status == 'SELESAI') {
                //user confirm transaction finish
                $from = $trx->userId;
                $to = $checkBuilding->managementId;
                $title = 'Transaksi Selesai';
                $desc = 'Transaksi Booking gedung telah selesai';
                $isTransaction = 1;
                $transactionId = $trx->transactionId;
                $buildingId = $trx->buildingId;
            } else if ($updatedBy == 'MANAGEMENT' && $status == 'BOOKING') {
                //management update trx to booking
                $from = $checkBuilding->managementId;
                $to = $trx->userId;
                $title = 'Transaksi Telah Diverifikasi';
                $desc = 'Selamat transaksi pembayaran sudah diverifikasi';
                $isTransaction = 1;
                $transactionId = $trx->transactionId;
                $buildingId = $trx->buildingId;
            } else if ($updatedBy == 'MANAGEMENT' && $status == 'CHECKIN') {
                //management update trx to checkin
                $from = $checkBuilding->managementId;
                $to = $trx->userId;
                $title = 'CHECKIN Gedung';
                $desc = 'Saat ini anda sudah melakukan Checkin gedung';
                $isTransaction = 1;
                $transactionId = $trx->transactionId;
                $buildingId = $trx->buildingId;
            } else if ($updatedBy == 'MANAGEMENT' && $status == 'SELESAI') {
                //management confirm transaction finish
                $from = $checkBuilding->managementId;
                $to = $trx->userId;
                $title = 'Transaksi Selesai';
                $desc = 'Booking gedung telah selesai, Ayo Berikan penilaianmu!';
                $isTransaction = 1;
                $transactionId = $trx->transactionId;
                $buildingId = $trx->buildingId;
            } else if ($updatedBy == 'MANAGEMENT' && $status == 'DITOLAK') {
                //management confirm transaction reject
                $from = $checkBuilding->managementId;
                $to = $trx->userId;
                $title = 'Transaksi Ditolak';
                $desc = 'Transaksi Pembayaran anda ditolak';
                $isTransaction = 1;
                $transactionId = $trx->transactionId;
                $buildingId = $trx->buildingId;
            }

            $dataSet = array (
                'from' => $from,
                'to' => $to,
                'isRead' => 0,
                'title' => $title,
                'description' => $desc,
                'isTransaction' => $isTransaction,
                'transactionId' => $transactionId,
                'buildingId' => $buildingId
            );

            // print_r( $dataSet); exit();
            // insert into table notification
            $this->doInputNotification($dataSet);
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

    public function getNotification($to)
    {
        try
        {
            $query = DB::table('notification');
            $query->where('to', $to);
            $query->where('isRead', 0);
            $query->orderBy('createdAt', 'desc');
            $count = $query->count();
            $notification = $query->get();

            $data = array(
                'count' => $count,
                'notification' => $notification,
            );

            return $data;

        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception($e);
        }
    }

    public function updateNotification($id)
    {
        if ($this->doCheckNotification($id) == null) {
            //validation notification id is not exist
            throw new ResponseException(ResponseConstants::NOTIFICATION_ID_NOT_EXIST);
        } else {
            try
            {
                DB::beginTransaction();

                // update table notification
                $this->doUpdateNotification($id);

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                throw new Exception($e);
            }
        }
    }

    private function doInputNotification($dataSet)
    {
        DB::table('notification')->insert($dataSet);
    }

    private function doCheckNotification($id)
    {
        $IdIsExist = DB::table('notification')->where("id", '=', $id)->first();
        return $IdIsExist;
    }

    private function doUpdateNotification($id)
    {
        DB::table('notification')->where('id', $id)->update(['isRead' => 1]);
    }

}
