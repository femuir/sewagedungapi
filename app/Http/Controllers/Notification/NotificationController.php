<?php

namespace App\Http\Controllers\Notification;

use App\Http\Constants\ResponseConstants;
use App\Http\Controllers\Controller;
use App\Http\Utils\ResponseException;
use Illuminate\Http\Request;
use Throwable;

class NotificationController extends Controller
{
    public function __construct()
    { }

    public function getNotification($to)
    {
        $status = [];
        $notification = [];

        try {
            $notificationService = new Notification();
            $notification = $notificationService->getNotification($to);

            $status = ResponseConstants::SUCCESS;
        } catch (ResponseException $th) {
            $status = $th->getResponse();
        } catch (Throwable $th) {
            $status = ResponseConstants::ERROR;
            $status['error_msg'] = $th->getMessage();
            $status['stactrace'] = $th->getTraceAsString();
        }

        $data = array(
            'msg' => $status['msg'],
            'notification' => $notification,
        );

        return response()->json($data, $status['status']);
    }

    public function updateNotification($id)
    {
        $data = [];
        
        try {
            $notificationService = new Notification();
            $notificationService->updateNotification($id);

            $data = ResponseConstants::UPDATE_NOTIFICATION;
        } catch (ResponseException $th) {
            $data = $th->getResponse();
        } catch (Throwable $th) {
            $data = ResponseConstants::ERROR;
            $data['error_msg'] = $th->getMessage();
            $data['stactrace'] = $th->getTraceAsString();
        }

        return response()->json($data, $data['status']);
    }
}
