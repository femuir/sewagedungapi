<?php

namespace App\Http\Controllers\User;

use App\Http\Constants\ResponseConstants;
use App\Http\Utils\ResponseException;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class User
{
    public function __construct()
    { }

    public function doRegister(Request $request)
    {
        $maxNumber = strlen((string)$request->phone);
        if ($this->doCheckEmailExistRegist($request->email)) {
            // check validation email is exist on system
            throw new ResponseException(ResponseConstants::REGISTRATION_EMAIL_ALREADY_EXISTS);
        } else if ($this->doCheckUser($request->userId)) {
            // check validation NIK is exist
            throw new ResponseException(ResponseConstants::REGISTRATION_NIK_ALREADY_EXISTS);
        } else if (($request->password != $request->confirm_password)) {
            throw new ResponseException(ResponseConstants::USER_PASSWORD_NOT_MATCH);
        } else if ($maxNumber > 12) {
            // check validation phone length
            throw new ResponseException(ResponseConstants::REGISTRATION_PHONE_LIMIT);
        } 
        else {
            try 
            {
                DB::beginTransaction();
    
                // insert into table user
                $this->doInsertUser($request);
                // insert into table user_detail
                $this->doInsertUserDetail($request);
                //get data user
                if ($request->role == 2) {
                    $user = $this->getDataUser($request->email);

                } else {
                    $user  = $this->getDataUserManagement($request->email);
                }
    
                DB::commit();

                return $user;
            } catch (Exception $e) {
                DB::rollBack();
                throw new Exception($e);
            }
        }
    }

    public function doLogin(Request $request)
    {
        $verifyUser = $this->doCheckEmailExistRegist($request->email);

        if ($verifyUser == null)
        {
            // validation user not found
            throw new ResponseException(ResponseConstants::LOGIN_INVALID);
        } else {
            try 
            {
                $verifyPass = password_verify($request->password, $verifyUser->password);

                if($verifyPass)
                {
                    if ($verifyUser->role == 2)
                    {
                        // get table user_detail
                        $user = $this->getDataUser($request->email);
                    } else if ($verifyUser->role == 1) {
                        // get table user_detail for manament building
                        $user  = $this->getDataUserManagement($request->email);
                    }
                    
                    return $user;
                } else {
                    // validation user not found
                    throw new ResponseException(ResponseConstants::LOGIN_INVALID);
                }

            } catch (Exception $e) {
                DB::rollBack();
                throw new Exception($e);
            }
        }
    }

    public function doGetProfil(Request $request)
    {
        $verifyUser = $this->doCheckEmailExistRegist($request->email);

        if ($verifyUser == null)
        {
            // validation user not found
            throw new ResponseException(ResponseConstants::USER_EMAIL_NOT_EXISTS);
        } else {
            try 
            {
                if ($verifyUser->role == 2)
                {
                    // get table user_detail
                    $user = $this->getDataUser($request->email);
                } else if ($verifyUser->role == 1) {
                    // get table user_detail for manament building
                    $user  = $this->getDataUserManagement($request->email);
                }
                
                return $user;

            } catch (Exception $e) {
                DB::rollBack();
                throw new Exception($e);
            }
        }
    }

    public function updateProfil($request)
    {
        if ($this->doCheckUser($request->userId) == null)
        {
            //validation user id is not exist
            throw new ResponseException(ResponseConstants::USER_ID_NOT_EXIST);
        } else {
            try 
            {
                DB::beginTransaction();

                // update table user_detail
                $this->doUpdateProfil($request);

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                throw new Exception($e);
            }
        }
    }

    public function updatePhotoProfil($request)
    {
        if ($this->doCheckUser($request->userId) == null)
        {
            //validation user id is not exist
            throw new ResponseException(ResponseConstants::USER_ID_NOT_EXIST);
        } else {
            try 
            {
                DB::beginTransaction();

                // update table user_detail
                $this->doUpdateProfil($request);

                // upload photo
                $this->doUploadPhoto($request, $request->userId);

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                throw new Exception($e);
            }
        }
    }

    public function resetPassword($request)
    {
        $verifyUser = $this->doCheckEmailExistRegist($request->email);
        // print_r(password_verify($request->password, $request->confirm_password)); exit;

        if ($verifyUser == null)
        {
            //validation email is not exist
            throw new ResponseException(ResponseConstants::USER_EMAIL_NOT_EXISTS);
        } else if ((password_verify($request->oldPassword, $verifyUser->password)) == null ) {
            //validation password is invalid
            throw new ResponseException(ResponseConstants::USER_INVALID_PASSWORD);
        } else if ( $request->password != $request->confirm_password) {
            //validation password is not match
            throw new ResponseException(ResponseConstants::USER_PASSWORD_NOT_MATCH);
        } else {
            try 
            {
                DB::beginTransaction();

                // update table user
                $this->doUpdateUser($request);

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                throw new Exception($e);
            }
        }
    }

    private function doCheckEmailExistRegist($email)
    {
        $isExistOnRegist = DB::table('user')->where("email", '=', $email)->first();
        return $isExistOnRegist;
    }

    public function doCheckUser($id)
    {
        $isExistUser = DB::table('user_detail')->where("userId", '=', $id)->first();
        return $isExistUser;
    }

    private function doInsertUser($user)
    {
        DB::table('user')->insert([
            'email' => $user->email,
            'password' => bcrypt($user->password),
            'role' => $user->role,
            'status' => 'A'
        ]);
    }

    private function doInsertUserDetail($user)
    {
        DB::table('user_detail')->insert([
            'userId' => $user->userId,
            'fullName' => $user->fullName,
            'email' => $user->email,
            'gender' => $user->gender,
            'phone' => $user->phone,
            'address' => $user->address,
        ]);
    }

    private function getDataUser($email)
    {
        $getUser = DB::table('user_detail');
        $getUser->select('user_detail.*', 'user.role', 'user.status');
        $getUser->leftjoin('user', 'user.email', '=', 'user_detail.email');
        $getUser->where("user_detail.email", '=', $email);
        $getUser = $getUser->first();
        
        return $getUser;
    }

    private function getDataUserManagement($email)
    {
        $getUser = DB::table('user_detail');
        $getUser->select('user_detail.*', 'user.role', 'user.status', 'building.buildingId');
        $getUser->leftjoin('user', 'user.email', '=', 'user_detail.email');
        $getUser->leftjoin('building', 'building.managementId', '=', 'user_detail.userId');
        $getUser->where("user_detail.email", '=', $email);
        $getUser = $getUser->first();
        
        return $getUser;
    }

    private function doUpdateProfil($profil)
    {
        $data = [];

        if (!empty($profil->input('fullName')))
            $data['fullName'] = $profil->input('fullName');
        if (!empty($profil->input('gender')))
            $data['gender'] = $profil->input('gender');
        if (!empty($profil->input('phone')))
            $data['phone'] = $profil->input('phone');
        if (!empty($profil->input('address')))
            $data['address'] = $profil->input('address');
        if (!empty($profil->file('picture')))
            $data['picture'] = $profil->file('picture')->getClientOriginalName();
        
        DB::table('user_detail')->where('userId', $profil->userId)->update($data);
    }

    private function doUploadPhoto($data, $id)
    {
        //inisiate variable
        $path = 'User/' . $id;
        $picture = $data->file('picture');

        //doupload
        if ($picture != null) {
            $picture->move($path, $picture->getClientOriginalName());
        }
    }

    private function doUpdateUser($user)
    {
        $data = [];
        date_default_timezone_set("Asia/Bangkok");
        $data['updateAt'] = Carbon::now()->toDateTimeString();
        $data['updateBy'] = $user->email;

        if (!empty($user->input('password')))
            $data['password'] = bcrypt($user->input('password'));
        if (!empty($user->input('role')))
            $data['role'] = $user->input('role');
        if (!empty($user->input('deleteAt')))
            $data['deleteAt'] = $user->input('deleteAt');
        if (!empty($user->input('deleteBy')))
            $data['deleteBy'] = $user->input('deleteBy');
        if (!empty($user->input('status')))
            $data['status'] = $user->input('status');
        
        DB::table('user')->where('email', $user->email)->update($data);
    }
}