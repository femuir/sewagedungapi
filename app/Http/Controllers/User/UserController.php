<?php

namespace App\Http\Controllers\User;

use App\Http\Constants\ResponseConstants;
use App\Http\Controllers\Controller;
use App\Http\Utils\ResponseException;
use Illuminate\Http\Request;
use Throwable;

class UserController extends Controller
{
    public function __construct()
    { }

    public function registerUser(Request $request)
    {
        $status = [];
        $getRegister = [];
        
        try {
            $userService = new User();
            $getRegister = $userService->doRegister($request);

            $status = ResponseConstants::REGISTRATION_SUCCESS;
        } catch (ResponseException $th) {
            $status = $th->getResponse();
        } catch (Throwable $th) {
            $status = ResponseConstants::ERROR;
            $status['error_msg'] = $th->getMessage();
            $status['stactrace'] = $th->getTraceAsString();
        }

        if ($status['status'] == 200){
            $data = array(
                'msg' => $status['msg'],
                'user' => $getRegister,
            );
        } else if ($status['status'] == 400)  {
            $data = array(
                'msg' => $status['msg'],
            );
        } else {
            $data = array(
                'msg' => $status['msg'],
                'error_msg' => $status['error_msg'],
                'stactrace' => $status['stactrace']
            );
        }

        return response()->json($data, $status['status']);
    }

    public function loginUser(Request $request)
    {
        $status = [];
        $getLogin = [];

        try {
            $userService = new User();
            $getLogin = $userService->doLogin($request);

            $status = ResponseConstants::LOGIN_SUCCESS;
        } catch (ResponseException $th) {
            $status = $th->getResponse();
        } catch (Throwable $th) {
            $status = ResponseConstants::ERROR;
            $status['error_msg'] = $th->getMessage();
            $status['stactrace'] = $th->getTraceAsString();
        }

        if ($status['status'] == 200){
            $data = array(
                'msg' => $status['msg'],
                'user' => $getLogin,
            );
        } else if ($status['status'] == 400)  {
            $data = array(
                'msg' => $status['msg'],
                'user' => $getLogin,
            );
        } else {
            $data = array(
                'msg' => $status['msg'],
                'user' => $getLogin,
                'error_msg' => $status['error_msg'],
                'stactrace' => $status['stactrace']
            );
        }
        
        return response()->json($data, $status['status']);
    }

    public function getProfil(Request $request)
    {
        $status = [];
        $getUser = [];

        try {
            $userService = new User();
            $getUser = $userService->doGetProfil($request);

            $status = ResponseConstants::SUCCESS;
        } catch (ResponseException $th) {
            $status = $th->getResponse();
        } catch (Throwable $th) {
            $status = ResponseConstants::ERROR;
            $status['error_msg'] = $th->getMessage();
            $status['stactrace'] = $th->getTraceAsString();
        }

        if ($status['status'] == 200){
            $data = array(
                'msg' => $status['msg'],
                'user' => $getUser,
            );
        } else if ($status['status'] == 400)  {
            $data = array(
                'msg' => $status['msg'],
                'user' => $getUser,
            );
        } else {
            $data = array(
                'msg' => $status['msg'],
                'user' => $getUser,
                'error_msg' => $status['error_msg'],
                'stactrace' => $status['stactrace']
            );
        }
        
        return response()->json($data, $status['status']);
    }


    public function updateProfil(Request $request)
    {
        $data = [];
        
        try {
            $userService = new User();
            $userService->updateProfil($request);

            $data = ResponseConstants::UPDATE_USER_SUCCESS;
        } catch (ResponseException $th) {
            $data = $th->getResponse();
        } catch (Throwable $th) {
            $data = ResponseConstants::ERROR;
            $data['error_msg'] = $th->getMessage();
            $data['stactrace'] = $th->getTraceAsString();
        }

        return response()->json($data, $data['status']);
    }

    public function updatePhotoProfil(Request $request)
    {
        $data = [];
        
        try {
            $userService = new User();
            $userService->updatePhotoProfil($request);

            $data = ResponseConstants::UPDATE_USER_SUCCESS;
        } catch (ResponseException $th) {
            $data = $th->getResponse();
        } catch (Throwable $th) {
            $data = ResponseConstants::ERROR;
            $data['error_msg'] = $th->getMessage();
            $data['stactrace'] = $th->getTraceAsString();
        }

        return response()->json($data, $data['status']);
    }

    public function resetPassword(Request $request)
    {
        $data = [];
        
        try {
            $userService = new User();
            $userService->resetPassword($request);

            $data = ResponseConstants::UPDATE_USER_SUCCESS;
        } catch (ResponseException $th) {
            $data = $th->getResponse();
        } catch (Throwable $th) {
            $data = ResponseConstants::ERROR;
            $data['error_msg'] = $th->getMessage();
            $data['stactrace'] = $th->getTraceAsString();
        }

        return response()->json($data, $data['status']);
    }
}
